import com.fursa.webparse.db.DBCrud;
import com.fursa.webparse.db.DBProperties;
import com.fursa.webparse.db.entity.Word;
import com.fursa.webparse.parser.HTMLParser;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

public class ApplicationTest {
    public static final String DATABASE_USER = "database_user";
    public static final String DATABASE_URL = "database_url";
    public static final String DATABASE_PASSWORD = "database_password";

    @Test
    public void saveToDatabase() throws SQLException, ClassNotFoundException {
        DBCrud crud = new DBCrud();
        Assert.assertEquals(1, crud.saveWord(new Word("Hello", 10)));
    }

    @Test
    public void isPropertiesCorrectlySaved() {
        DBProperties properties = new DBProperties();
        Map<String, String> propertiesMap = properties.loadProperties();
        Assert.assertEquals(propertiesMap.get(DATABASE_USER), "postgres");
        Assert.assertEquals(propertiesMap.get(DATABASE_URL), "jdbc:postgresql://localhost:5432/words");
        Assert.assertEquals(propertiesMap.get(DATABASE_PASSWORD), "toor");
    }

    @Test
    public void testVKPageTitleText() throws SQLException, IOException, ClassNotFoundException {
        HTMLParser parser = new HTMLParser("https://vk.com");
        Assert.assertEquals("Мобильная версия ВКонтакте", parser.getPageTitle());
    }

}
