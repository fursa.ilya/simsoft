package com.fursa.webparse.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Проверка валидности адреса сайта
 */

public class UrlValidatorUtil {
    private static final String URL_VALIDATION_REGEX = "^(https?|http)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    /**
     * @param webUrl - входной параметр URL вебсайта
     * @return boolean true - значит URL корректный, false - нет!
     */
    public static boolean isUrlCorrect(String webUrl) {
        Pattern urlValidationPattern = Pattern.compile(URL_VALIDATION_REGEX);
        Matcher urlValidationMatcher = urlValidationPattern.matcher(webUrl);

        return urlValidationMatcher.matches();
    }
}
