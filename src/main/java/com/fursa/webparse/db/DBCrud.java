package com.fursa.webparse.db;

import com.fursa.webparse.db.entity.Word;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
    wordId SERIAL in postgres means AUTOINCREMENT
	word TEXT
	repeat INT
*/
public class DBCrud {
    private static Connection connection;

    public DBCrud() throws SQLException, ClassNotFoundException {
        connection = PsgConnection.newInstance();
    }

    public int saveWord(Word word) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("insert into wordstat(word, repeat) values (?,?)");
        ps.setString(1, word.getWord());
        ps.setInt(2, word.getRepeatCount());

        return ps.executeUpdate();
    }
}
