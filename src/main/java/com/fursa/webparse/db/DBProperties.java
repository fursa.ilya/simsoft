package com.fursa.webparse.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class DBProperties {
    public static final String DATABASE_USER = "database_user";
    public static final String DATABASE_URL = "database_url";
    public static final String DATABASE_PASSWORD = "database_password";
    private Properties properties;

    public DBProperties() { }

    public Map<String, String> loadProperties() {
        Map<String, String> propertiesMap = new HashMap<>();

        try (InputStream is = getClass().getClassLoader().getResourceAsStream("app.properties")) {

            properties = new Properties();

            if (is == null) {
                System.out.println("[-] Can't load properties");
            }

            properties.load(is);

            propertiesMap.put(DATABASE_USER, properties.getProperty(DATABASE_USER));
            propertiesMap.put(DATABASE_PASSWORD, properties.getProperty(DATABASE_PASSWORD));
            propertiesMap.put(DATABASE_URL, properties.getProperty(DATABASE_URL));


        } catch (IOException e) {
            e.printStackTrace();
        }

        return propertiesMap;
    }
}
