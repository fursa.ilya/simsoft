package com.fursa.webparse.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import static com.fursa.webparse.db.DBProperties.*;

public class PsgConnection {

    private static final String PLSQL_DRIVER = "org.postgresql.Driver";

    private static DBProperties properties = new DBProperties();
    private static Map<String, String> propertiesMap = properties.loadProperties();

    private static Connection connection = null;

    private PsgConnection() { }

    public static Connection newInstance() throws ClassNotFoundException, SQLException {

        Class.forName(PLSQL_DRIVER);

        if(connection == null) {
            connection = DriverManager.getConnection(
                    propertiesMap.get(DATABASE_URL),
                    propertiesMap.get(DATABASE_USER),
                    propertiesMap.get(DATABASE_PASSWORD));
        }

        return connection;
    }

    public void closeConnection() throws SQLException { connection.close(); }

}
