package com.fursa.webparse.db.entity;

public class Word {
    private String word;
    private int repeatCount;

    public Word(String word, int repeatCount) {
        this.word = word;
        this.repeatCount = repeatCount;
    }

    public String getWord() {
        return word;
    }

    public int getRepeatCount() {
        return repeatCount;
    }

}
