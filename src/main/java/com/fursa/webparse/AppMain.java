package com.fursa.webparse;

import com.fursa.webparse.parser.HTMLParser;
import com.fursa.webparse.parser.PageDownloader;
import com.fursa.webparse.util.UrlValidatorUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

@SpringBootApplication
public class AppMain {

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        SpringApplication.run(AppMain.class, args);

        String[] htmlTags = {"a", "p",
                "title", "h1", "h2", "h3",
                "h4", "h5", "h6", "b", "i",
                "u", "abbr", "input", "div"};

        Scanner sc = new Scanner(System.in);
        System.out.print("[!]Введите URL сайта с префиксом http или https: ");
        String websiteUrl = sc.nextLine();
        System.out.println();

        if(UrlValidatorUtil.isUrlCorrect(websiteUrl)) {
            System.out.print("[!]Имя файла(название сайта) для сохранения на жестком диске: ");
            String websiteOutFileName = sc.nextLine();

            PageDownloader pageDownloader = new PageDownloader(websiteUrl, websiteOutFileName);
            pageDownloader.downloadWebsiteHtml();

            HTMLParser htmlParser = new HTMLParser(websiteUrl);

            for (String htmlTag : htmlTags) {
                htmlParser.getElementText(htmlTag);
            }

            htmlParser.printResult();

        } else {
            System.out.println("[!]Неверный URL сайта!");
        }

    }








}

