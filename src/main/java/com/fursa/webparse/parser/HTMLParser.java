package com.fursa.webparse.parser;

import com.fursa.webparse.db.DBCrud;
import com.fursa.webparse.db.entity.Word;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class HTMLParser {
    private static final String DEFAULT_REFERRER_URL = "http://www.google.com";
    private static final String DEFAULT_USER_AGENT = "Chrome/4.0.249.0 Safari/532.5";

    private final DBCrud dbCrud = new DBCrud();

    private Document doc;
    private List<String> words = new ArrayList<>();
    private Map<String, Integer> resultMap = new HashMap<>();

    public HTMLParser(String webUrl) throws IOException, SQLException, ClassNotFoundException {
        doc = Jsoup.connect(webUrl)
                .userAgent(DEFAULT_USER_AGENT)
                .referrer(DEFAULT_REFERRER_URL)
                .get();
    }

    public void getElementText(String tag) {
        for (Element e : doc.getAllElements().select(tag)) {
            if (!e.text().isEmpty()) {
                String[] str = e.text().split(" ");
                words.addAll(Arrays.asList(str));
            }

            if(tag.equals("title") && !doc.title().isEmpty()) {
                String[] title = doc.title().split(" ");
                words.addAll(Arrays.asList(title));
            }
        }

        for (String word : words) {
            resultMap.put(word, Collections.frequency(words, word));
        }

    }

    public String getPageTitle() {
        return doc.title();
    }

    public void printResult() throws SQLException {
        for (Map.Entry<String, Integer> entry : resultMap.entrySet()) {
            System.out.println("[+] got a new word " + entry.getKey() + " which repeats " + entry.getValue() + " times");
            dbCrud.saveWord(new Word(entry.getKey(), entry.getValue()));
        }
    }


}
