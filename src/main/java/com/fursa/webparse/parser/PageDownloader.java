package com.fursa.webparse.parser;

import com.fursa.webparse.util.UrlValidatorUtil;

import java.io.*;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Класс для скачивания веб страницы.
 * Конструктор получает URL веб страницы и имя выходного файла.
 * Сохраняет страницу в формате .html
 *
 */

public class PageDownloader {
    private BufferedWriter bufferedWriter;
    private BufferedReader bufferedReader;
    /**
     * @param webUrl - url веб - страницы для скачивания
     * @param outFileName - имя выходного файла
     * @throws IOException
     */
    public PageDownloader(String webUrl, String outFileName) throws IOException {

        if(UrlValidatorUtil.isUrlCorrect(webUrl)) {
            URL websiteUrl = new URL(webUrl);

            try {
                bufferedReader = new BufferedReader(new InputStreamReader(websiteUrl.openStream()));
                bufferedWriter = new BufferedWriter(new FileWriter(outFileName + ".html"));
            } catch (UnknownHostException e) {
                System.out.println("[-] Неизвестное имя хоста");
            } finally {
                System.out.println("[-] Выполнение было прервано");
            }
        }
    }

    public void downloadWebsiteHtml() throws IOException {
        String currentTextLine;
        while ((currentTextLine = bufferedReader.readLine()) != null) {
            bufferedWriter.write(currentTextLine);
            bufferedWriter.newLine();
        }

        bufferedReader.close();
        bufferedWriter.close();
    }

}
